#!/usr/bin/python3

import requests
import json
import simplekml

#Set filename/path for KML file output
kmlfile = "croixrouge.kml"
#Set KML schema name
kmlschemaname = "croixrouge"
#Set API URL
apiURL = "https://maisons.croix-rouge.be/wp-json/redcrosshouses/v1/service/315"
#Surround payload with three double quotes in order for Python to keep all the double quotes intact for retaining valid JSON, and modify the query to 400 hitsPerPage to get all store data in one call
#payload = """{"requests":[{"indexName":"wp_posts_post","params":"query=&hitsPerPage=400&facets=%5B%22taxonomies.acteurs%22%2C%22taxonomies.produits%22%2C%22taxonomies.villes%22%2C%22taxonomies.cp%22%2C%22taxonomies.province%22%5D&tagFilters=&facetFilters=%5B%5B%22taxonomies.acteurs%3APoint%20de%20vente%22%5D%5D"}]}"""
#Get the API response and JSONify it
#response = requests.post(apiURL,data=payload)
response = requests.get(apiURL)
mapjson = response.json()
#example JSON {'title': {'rendered': 'Ourthe et Aisne'}, 'entities_id': 4881, 'address': "Rue d'Erezée, 6 - 6960 Manhay", 'location': ['50.29233', '5.6510137'], 'link': 'https://maisons.croix-rouge.be/maisons/ourthe-et-aisne/service/4881/', 'type': 'rc_service_entities'}
#Initialize kml object
kml = simplekml.Kml()
#Add schema, which is required for a custom address field
schema = kml.newschema(name=kmlschemaname)
schema.newsimplefield(name="address",type="string")

#Iterate through JSON for each store
for item in mapjson:
    #Get the name of the store
    storename = item["title"]["rendered"]
    #Get the store address
    storeaddress = item["address"]
    #Set the coordinates
    lat = item["location"][0]
    lng = item["location"][1]
    #First, create the point name and description in the kml
    point = kml.newpoint(name=storename,description="thrift store")
    #Then, add the custom "SimpleData" address field to kml file with proper hierarchical kml data structure
    point.extendeddata.schemadata.schemaurl = kmlschemaname
    simpledata = point.extendeddata.schemadata.newsimpledata("address",storeaddress)
    #Finally, add coordinates to the feature
    point.coords=[(lng, lat)]
#Save the final KML file
kml.save(kmlfile)
