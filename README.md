# Croix-Rouge Belgium Location Extractor
Extracts Croix-Rouge Vestiboutique locations from the official website's API and places them into KML format.

## Dependencies
* Python 3.x
    * Requests module for calling the API
    * JSON for parsing the API response
    * Simplekml for easily building KML files
* Also of course depends on official [Croix-Rouge de Belgique](https://vestiboutiques.croix-rouge.be) website.
